FROM node:12.18-alpine
#RUN apk add --no-cache python2 g++ make
WORKDIR /app

COPY . .

RUN npm install && npm install -g pm2

CMD ["pm2-runtime", "ecosystem.config.js"]
#CMD ["npm", "start"]
